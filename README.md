# Boost 1.58 #

Boost 1.58 libraries (static libs and dlls), compiled with Visual Studio 2015 (v140;Win32,x64;debug,release)
I would avoid autolinking (+= BOOST_ALL_NO_LIB), and stick to mt dlls (+= BOOST_ALL_DYN_LINK)
Can save developer some time

* vlad.serhiienko@gmail.com
* http://www.boost.org/users/history/version_1_58_0.html